const { response } = require('express')

// Helpers
const { matchPwd } = require('../helpers/bcryptjs.helper')
const { getUserByEmail, saveUser } = require('../helpers/querys/user.helper')
const { googleVerify } = require('../helpers/googleAuth.helper')
const { getJWT } = require('../helpers/jwt.helpers')
const { bankCode } = require('../helpers/bankCode.helper')

const postLogin = async (req, res = response) => {
  const { email, password } = req.body

  try {
    // valid email user
    const user = await getUserByEmail(email)
    if (!user) {
      return res.status(400).json({ error: bankCode('user-not-exist') })
    }

    // valid status user
    if (!user.state) {
      return res.status(400).json({ error: bankCode('access-denied') })
    }

    // valid password
    const validPwd = matchPwd(password, user.password)
    if (!validPwd) {
      return res.status(400).json({ error: bankCode('credentials-invalid') })
    }

    // get JWT
    const token = await getJWT(user.id)

    res.json({ user, token })
  } catch (error) {
    console.error(error)
    res.status(500).json({ error: bankCode('internal-error') })
  }
}

const postGoogleAuth = async (req, res) => {
  const { id_token: IdToken } = req.body

  try {
    const { email, name, img } = await googleVerify(IdToken)

    let user = await getUserByEmail(email)

    if (!user) {
      const data = { name, email, img, google: true, password: '' }
      user = await saveUser(data)
    }

    if (!user.state) {
      return res.status(401).json({ error: bankCode('access-denied') })
    }

    const token = await getJWT(user.id)

    res.json({ token })
  } catch (error) {
    console.log(error)

    res.status(400).json({
      error: bankCode('invalid-authentication')
    })
  }
}

module.exports = { postLogin, postGoogleAuth }
