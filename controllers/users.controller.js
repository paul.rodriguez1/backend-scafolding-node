const { response } = require('express')

// helpers
const { getSalt } = require('../helpers/bcryptjs.helper')
const { saveUser } = require('../helpers/querys/user.helper')

// Model
const UserModel = require('../models/user.model')

const getUsers = async (req, res = response) => {
  const { limit = 5, page = 1 } = req.query

  const Limit = parseInt(limit)
  const Page = parseInt(page > 0 ? page : 1)
  const skip = Page > 1 ? (Limit * Page) - Limit : 0

  const query = { state: true }

  const [total, users] = await Promise.all([
    UserModel.countDocuments(query),
    UserModel.find(query)
      .skip(skip)
      .limit(Limit)

  ])

  res.json({ total, users })
}

const getUser = async (req, res = response) => {
  const { id } = req.params
  const user = await UserModel.findById(id)
  res.json({ user })
}

const putUser = async (req, res = response) => {
  const { id } = req.params
  const { _id, password, google, ...body } = req.body

  // Valid info to Save.
  if (password) {
    body.password = getSalt(password)
  }
  const User = await UserModel.findByIdAndUpdate(id, body)

  res.json({ User })
}

const postUser = async (req, res = response) => {
  const user = await saveUser(req.body)
  res.json({ user })
}

const deleteUser = async (req, res = response) => {
  const { id } = req.params
  await UserModel.findByIdAndUpdate(id, { state: false })
  res.json({ user })
}

const patchUser = (req, res = response) => {
  res.json({ data: 'patch info -- Controller' })
}

module.exports = {
  getUsers,
  getUser,
  putUser,
  postUser,
  deleteUser,
  patchUser
}
