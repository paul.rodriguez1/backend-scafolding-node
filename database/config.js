const mongoose = require('mongoose')

const dbConnection = async () => {
  try {
    await mongoose.connect(process.env.MONGODB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    console.log('Database is connected')
  } catch (error) {
    console.log(error)

    throw new Error('Error en la conexion de la base, consulte con el administrador del sistema')
  }
}

module.exports = {
  dbConnection
}
