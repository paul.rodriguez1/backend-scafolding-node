const UserModel = require('../../models/user.model')
const { getSalt } = require('../bcryptjs.helper')

const validEmail = async (email = '') => {
  const emailExist = await UserModel.findOne({ email })
  if (emailExist) {
    throw new Error(`El e-mail ${email} ya se encuentra registrado.`)
  }
}

const validUserExistId = async (id = '') => {
  const idExist = await UserModel.findById(id)
  if (!idExist) {
    throw new Error(`El id ${id} no es válido.`)
  }
}

const getUserById = async (id = '') => {
  const user = await UserModel.findById(id)

  if (!user) {
    throw new Error(`El id ${id} no es válido.`)
  } else {
    return user
  }
}

const getUserByEmail = async (email = '') => {
  const user = await UserModel.findOne({ email })
  if (!user) {
    throw new Error(`El email ${email} no es válido.`)
  } else {
    return user
  }
}

const saveUser = async (data) => {
  const { password } = data
  console.log(data)

  const user = new UserModel(data)

  // Encrypt password
  user.password = getSalt(password)

  // Save user
  user.save()
  return user
}

module.exports = { validEmail, validUserExistId, getUserByEmail, getUserById, saveUser }
