const rolModel = require('../../models/rol.model')

const validRoles = async (rol = '') => {
  const rolExist = await rolModel.findOne({ rol })
  if (!rolExist) {
    throw new Error(`El ${rol} no es válido.`)
  }
}

module.exports = { validRoles }
