const jwt = require('jsonwebtoken')

const getJWT = (uid = '') => {
  return new Promise((resolve, reject) => {
    const payload = { uid }
    jwt.sign(payload, process.env.SECRECT_JWT, {
      expiresIn: '4h'
    }, (err, token) => {
      if (err) {
        console.log(err)
        reject(new Error('No se pudo generar el Token'))
      } else {
        resolve(token)
      }
    })
  })
}

module.exports = { getJWT }
