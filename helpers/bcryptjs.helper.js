const bcryptjs = require('bcryptjs')

const getSalt = (pass) => {
  const salt = bcryptjs.genSaltSync()
  return bcryptjs.hashSync(pass, salt)
}

const matchPwd = (pwd, dbPwd) => {
  return bcryptjs.compareSync(pwd, dbPwd)
}

module.exports = { getSalt, matchPwd }
