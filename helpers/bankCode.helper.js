const bankCode = () => {
  const listCode = {
    // Login & User
    'user-not-exist': 'Usuario no existe o esta inactivo',
    'credentials-invalid': 'Credeciales no válidas',
    'internal-error': 'Se ha generado una novedad en el sistema, por favor comuniquese con el administrador del sistema.',
    'access-denied': 'Acceso denegado',
    'invalid-authentication': 'Autenticación no válida'

  }
  return listCode[value] || ''
}

module.exports = { bankCode }
