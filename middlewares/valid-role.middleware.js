const { bankCode } = require('../helpers/bankCode.helper')

const validRoleAdmin = (req, res, next) => {
  if (!req.user) {
    return res.status(500).json({ error: bankCode('invalid-authentication') })
  }
  const { role } = req.user

  if (role !== 'ADMIN_ROLE') {
    return res.status(401).json({ error: bankCode('access-denied') })
  }

  next()
}

module.exports = {
  validRoleAdmin
}
