const jwt = require('jsonwebtoken')
const { bankCode } = require('../helpers/bankCode.helper')
const { getUserById } = require('../helpers/querys/user.helper')

const validJWT = async (req, res, next) => {
  const token = req.header('x-token')

  if (!token) {
    return res.status(401).json({ error: bankCode('access-denied') })
  }

  try {
    const { uid } = jwt.verify(token, process.env.SECRECT_JWT)

    // valid id to User
    const user = await getUserById(uid)
    req.user = user

    if (!user.state) {
      return res.status(401).json({ error: bankCode('invalid-authentication') })
    }

    next()
  } catch (error) {
    console.log(error)
    res.status(401).json({ error: bankCode('invalid-authentication') })
  }
}

module.exports = {
  validJWT
}
