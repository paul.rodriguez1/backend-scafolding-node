// Middlewares
const validFields = require('./valid-fields.middleware')
const validJWT = require('./valid-jwt.middleware')
const validRole = require('./valid-role.middleware')

module.exports = { ...validFields, ...validJWT, ...validRole }
