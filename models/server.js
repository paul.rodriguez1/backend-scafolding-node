const express = require('express')
const cors = require('cors')
const { dbConnection } = require('../database/config')

class Server {
  constructor () {
    this.app = express()
    this.port = process.env.PORT
    this.pathUsers = '/api/users'
    this.pathAuth = '/api/auth'

    this.connectionsDB()
    this.middleware()
    this.routes()
  }

  // Conexion DB
  async connectionsDB () {
    await dbConnection()
  }

  // MIddleware
  middleware () {
    this.app.use(cors())
    this.app.use(express.json())
    this.app.use(express.static('public'))
  }

  // Routes
  routes () {
    this.app.use(this.pathUsers, require('../routes/users.router'))
    this.app.use(this.pathAuth, require('../routes/auth.router'))
  }

  startServer () {
    this.app.listen(this.port, () =>
      console.log(`Server listening ${this.port}`)
    )
  }
}

module.exports = Server
