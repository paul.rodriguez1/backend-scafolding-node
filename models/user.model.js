const { Schema, model } = require('mongoose')

const UserSchema = Schema({
  name: {
    type: String,
    require: [true, 'El nombre es requerido']
  },
  email: {
    type: String,
    require: [true, 'El correo  es requerido'],
    unique: true
  },
  password: {
    type: String,
    require: [true, 'La contraseña es requerida']
  },
  img: {
    type: String
  },
  role: {
    type: String,
    require: true,
    emun: ['ADMIN_ROLE', 'USER_ROLE']
  },
  state: {
    type: Boolean,
    default: true
  },
  google: {
    type: Boolean,
    default: false
  }

})

UserSchema.methods.toJSON = function () {
  const { __v, password, _id: uid, ...user } = this.toObject()
  return { uid, ...user }
}

module.exports = model('User', UserSchema)
