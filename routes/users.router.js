const { Router } = require('express')
const { check } = require('express-validator')
const router = Router()

// // Middlewares
const { validFields, validJWT, validRoleAdmin } = require('../middlewares')

// Controllers
const { getUsers, putUser, postUser, deleteUser, patchUser, getUser } = require('../controllers/users.controller')

// Helpers
const { validEmail, validUserExistId } = require('../helpers/querys/user.helper')
const { validRoles } = require('../helpers/querys/roles.helpers')

router.get('/', getUsers)

router.get('/:id', [
  check('id', 'El valor no es válido').isMongoId(),
  check('id').custom(validUserExistId),
  validFields
], getUser)

router.put('/:id', [
  check('id', 'El valor no es válido').isMongoId(),
  check('id').custom(validUserExistId),
  check('role').custom(validRoles),
  validFields
], putUser)

router.post('/', [
  check('name', 'El valor de name, es requerido').not().isEmpty(),
  check('password', 'El valor del password, debe ser superior o igual a 6 caracteres').isLength({ min: 6 }),
  check('email', 'El E-mail no tiene el formato adecuado <example@email.com>').isEmail(),
  check('email').custom(validEmail),
  check('role').custom(validRoles),
  validFields
], postUser)

router.delete('/:id', [
  validJWT,
  validRoleAdmin,
  check('id', 'El valor no es válido').isMongoId(),
  check('id').custom(validUserExistId),

  validFields
], deleteUser)

router.patch('/', patchUser)

module.exports = router
