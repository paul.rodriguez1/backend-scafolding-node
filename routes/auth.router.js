const { Router } = require('express')
const { check } = require('express-validator')
const router = Router()

// Controllers
const { postLogin, postGoogleAuth } = require('../controllers/auth.controller')

// Middlewares
const { validFields } = require('../middlewares/valid-fields.middleware')

router.post('/login', [
  check('email', 'El email es requerido').not().isEmpty(),
  check('email', 'El email no es válido').isEmail(),
  check('password', 'La contraseña es requerida').not().isEmpty(),
  validFields
], postLogin)

router.post('/googleAuth', [
  check('id_token', 'El token Google es requerido').not().isEmpty(),
  validFields
], postGoogleAuth)

module.exports = router
